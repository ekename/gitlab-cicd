#!/bin/bash

gitlabrunner=https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
golang=https://dl.google.com/go/go1.13.4.linux-amd64.tar.gz

apt update
apt install -y wget git curl tmux psmisc

wget $gitlabrunner -O gitlab-runner.deb
dpkg -i gitlab-runner.deb
rm gitlab-runner.deb
gitlab-runner start & 

wget $golang -O golang.tar.gz
tar -C /usr/local -xzf golang.tar.gz
rm golang.tar.gz
ln -s /usr/local/go/bin/go /usr/bin/go
